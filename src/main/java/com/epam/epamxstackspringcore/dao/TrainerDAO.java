package com.epam.epamxstackspringcore.dao;

import com.epam.epamxstackspringcore.config.InMemoryStorage;
import com.epam.epamxstackspringcore.entity.Trainer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@RequiredArgsConstructor
@Slf4j
public class TrainerDAO {
    private final InMemoryStorage inMemoryStorage;
    private Map<Long, Trainer> trainerMap;
    public void createTrainer(Trainer trainer) {
        trainerMap = inMemoryStorage.getTrainers();
        Long id = Long.valueOf(trainerMap.size() + 1);
        trainer.setId(id.toString());
        trainerMap.put(id, trainer);
    }

    public Trainer getTrainerById(String id){
        trainerMap = inMemoryStorage.getTrainers();
        return trainerMap.get(Long.valueOf(id));
    }

    public List<Trainer> getTrainers() {
        trainerMap = inMemoryStorage.getTrainers();
        return trainerMap.values().stream().toList();
    }

}
