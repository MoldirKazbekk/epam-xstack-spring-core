package com.epam.epamxstackspringcore.dao;

import com.epam.epamxstackspringcore.config.InMemoryStorage;
import com.epam.epamxstackspringcore.entity.TrainingType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@RequiredArgsConstructor
public class TrainingTypeDAO {
    private final InMemoryStorage inMemoryStorage;
    private Map<Long, TrainingType> trainingTypeMap;

    public TrainingType getTrainingTypeById(String id) {
        trainingTypeMap = inMemoryStorage.getTrainingTypes();
        return trainingTypeMap.get(Long.valueOf(id));
    }
}
