package com.epam.epamxstackspringcore.dao;

import com.epam.epamxstackspringcore.config.InMemoryStorage;
import com.epam.epamxstackspringcore.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Repository
@RequiredArgsConstructor
public class UserDAO {
    private final InMemoryStorage inMemoryStorage;
    private Map<Long, User> userMap;

    public User createUser(User user) {
        userMap = inMemoryStorage.getUsers();
        Long id = Long.valueOf(userMap.size() + 1);
        user.setId(id.toString());
        userMap.put(id, user);
        return user;
    }

    public List<User> getUsers() {
        userMap = inMemoryStorage.getUsers();
        return userMap.values().stream().toList();
    }

    public User getUserById(String id) {
        userMap = inMemoryStorage.getUsers();
        return userMap.get(Long.valueOf(id));
    }

    public boolean deleteUserById(String userId) {
        userMap = inMemoryStorage.getUsers();
        User user = userMap.get(Long.valueOf(userId));
        if (user == null) {
            return false;
        }
        user.setIsActive(false);
        return true;
    }

    public List<User> getUsersByFirstAndLastNames(String firstName, String lastName) {
        userMap = inMemoryStorage.getUsers();
        List<User> users = new ArrayList<>();
        for (User user : userMap.values()) {
            if (user.getFirstName().equals(firstName) && user.getLastName().equals(lastName)) {
                users.add(user);
            }
        }
        return users;
    }
}
