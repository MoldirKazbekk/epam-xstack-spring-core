package com.epam.epamxstackspringcore.dao;

import com.epam.epamxstackspringcore.config.InMemoryStorage;
import com.epam.epamxstackspringcore.entity.Training;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@RequiredArgsConstructor
@Slf4j
public class TrainingDAO {
    private final InMemoryStorage inMemoryStorage;
    private Map<Long, Training> trainingMap;

    public void createTraining(Training training) {
        trainingMap = inMemoryStorage.getTrainings();
        Long id = Long.valueOf(trainingMap.size() + 1);
        training.setId(id.toString());
        trainingMap.put(Long.valueOf(id), training);
        log.info("training record {} is created successfully", training);
    }

    public List<Training> getTrainings() {
        trainingMap = inMemoryStorage.getTrainings();
        return trainingMap.values().stream().toList();
    }
}
