package com.epam.epamxstackspringcore.dao;

import com.epam.epamxstackspringcore.config.InMemoryStorage;
import com.epam.epamxstackspringcore.entity.Trainee;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
@RequiredArgsConstructor
@Slf4j
public class TraineeDAO {
    private final InMemoryStorage inMemoryStorage;
    private Map<Long, Trainee> traineeMap;

    public void createTrainee(Trainee trainee) {
        traineeMap = inMemoryStorage.getTrainees();
        Long id = Long.valueOf(traineeMap.size() + 1);
        trainee.setId(id.toString());
        traineeMap.put(Long.valueOf(id), trainee);
    }

    public List<Trainee> getTrainees() {
        traineeMap = inMemoryStorage.getTrainees();
        return traineeMap.values().stream().toList();
    }

    public Trainee getTraineeById(String id) {
        traineeMap = inMemoryStorage.getTrainees();
        return traineeMap.get(Long.valueOf(id));
    }

    public Trainee deleteTraineeById(String traineeId) {
        traineeMap = inMemoryStorage.getTrainees();
        Trainee trainee = traineeMap.remove(Long.valueOf(traineeId));
        return trainee;
    }
}
