package com.epam.epamxstackspringcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpamXstackSpringCoreApplication {
	public static void main(String[] args) {
		SpringApplication.run(EpamXstackSpringCoreApplication.class, args);
	}

}
