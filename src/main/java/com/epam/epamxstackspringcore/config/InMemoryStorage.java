package com.epam.epamxstackspringcore.config;

import com.epam.epamxstackspringcore.entity.*;
import jakarta.annotation.PostConstruct;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
@Component
@Getter
@Slf4j
public class InMemoryStorage {
    @Value("${user.data.file}")
    private String userDataFilePath;

    @Value("${trainee.data.file}")
    private String traineeDataFilePath;

    @Value("${training.data.file}")
    private String trainingDataFilePath;

    @Value("${trainingType.data.file}")
    private String trainingTypeDataFilePath;

    @Value("${trainer.data.file}")
    private String trainerDataFilePath;

    private Map<Long, Trainer> trainers = new HashMap<>();

    private Map<Long, User> users = new HashMap<>();

    private Map<Long, Trainee> trainees = new HashMap<>();

    private Map<Long, Training> trainings = new HashMap<>();

    private Map<Long, TrainingType> trainingTypes = new HashMap<>();
    @PostConstruct
    public void initializeStorageWithDataFromFile(){
        users = readDataFromFile(userDataFilePath, User.class);
        trainees = readDataFromFile(traineeDataFilePath, Trainee.class);
        trainings = readDataFromFile(trainingDataFilePath, Training.class);
        trainingTypes= readDataFromFile(trainingTypeDataFilePath, TrainingType.class);
        trainers = readDataFromFile(trainerDataFilePath, Trainer.class);
    }
    public <T> Map<Long, T> readDataFromFile(String filePath, Class<T> entityClass){
        Map<Long, T> dataMap = new HashMap<>();
        ClassLoader classLoader = getClass().getClassLoader();
        log.info("file path: {}", filePath);
        File file = new File(classLoader.getResource(filePath).getFile());
        try (FileReader reader = new FileReader(file);
             CSVParser csvParser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(reader)) {
            for (CSVRecord csvRecord : csvParser) {
                // Parse and create the entity object
                T entity = createEntityFromCSVRecord(csvRecord, entityClass);
                // Store the entity in the map
                assert entity != null;
                dataMap.put(Long.valueOf((entity.getClass().getMethod("getId").invoke(entity)).toString()), entity);
            }
        } catch (IOException | ReflectiveOperationException e) {
            e.printStackTrace();
        }
        return dataMap;
    }

    private <T> T createEntityFromCSVRecord(CSVRecord csvRecord, Class<T> entityClass) {
        try {
            // Create a new instance of the entity class using the no-arg constructor
            T entity = entityClass.getDeclaredConstructor().newInstance();

            // Iterate through the columns in the CSV record and set corresponding properties of the entity
            for (String columnName : csvRecord.toMap().keySet()) {
                // Construct the setter method name for the column (assuming camelCase property names)
                String setterName = "set" + columnName.substring(0, 1).toUpperCase() + columnName.substring(1);
                // Get the setter method of the entity class
                java.lang.reflect.Method setter = entityClass.getMethod(setterName, String.class);

                // Set the property using the setter method
                setter.invoke(entity, csvRecord.get(columnName));
            }

            return entity;
        } catch (Exception e) {
            e.printStackTrace();
            return null; // Handle exceptions appropriately for your application
        }
    }
}
//try (FileReader reader = new FileReader(fileName);
//        CSVParser csvParser = CSVFormat.DEFAULT.withFirstRecordAsHeader().parse(reader)) {
//
//        for (CSVRecord csvRecord : csvParser) {
//        String entity = csvRecord.get("entity");
//
//        switch (entity) {
//        case "User":
//        User user = new User();
//        user.setId(Long.parseLong(csvRecord.get("id")));
//        user.setFirstName(csvRecord.get("firstName"));
//        user.setLastName(csvRecord.get("lastName"));
//        user.setUsername(csvRecord.get("username"));
//        user.setPassword(csvRecord.get("password"));
//        user.setIsActive(Boolean.parseBoolean(csvRecord.get("isActive")));
//        users.put(user.getId(), user);
//        break;
//
//        case "Trainee":
//        Trainee trainee = new Trainee();
//        trainee.setId(Long.parseLong(csvRecord.get("id")));
//        trainee.setAddress(csvRecord.get("address"));
//        trainee.setDateOfBirth(Date.valueOf(csvRecord.get("dateOfBirth")));
//        trainees.put(trainee.getId(), trainee);
//        break;
//
//        case "Training":
//        Training training = new Training();
//        training.setId(Long.parseLong(csvRecord.get("id")));
//        training.setTrainingTypeId(Long.parseLong(csvRecord.get("trainingTypeId")));
//        training.setTraineeId(Long.parseLong(csvRecord.get("traineeId")));
//        training.setTrainerId(Long.parseLong(csvRecord.get("trainerId")));
//        training.setName(csvRecord.get("name"));
//        training.setDate(Date.valueOf(csvRecord.get("date")));
//        training.setDuration(Long.valueOf(csvRecord.get("duration")));
//        trainings.put(training.getId(),training);
//        break;
//
//        case "TrainingType":
//        TrainingType trainingType = new TrainingType();
//        trainingType.setId(Long.parseLong(csvRecord.get("id")));
//        trainingType.setTypeName(csvRecord.get("trainingTypeName"));
//        trainingTypes.put(trainingType.getId(), trainingType);
//        break;
//
//        case "Trainer":
//        Trainer trainer = new Trainer();
//        trainer.setId(Long.parseLong(csvRecord.get("id")));
//        trainer.setSpecialization(csvRecord.get("specialization"));
//        trainer.setUserId(Long.valueOf(csvRecord.get("userId")));
//        // Set other Trainer fields here
//        trainers.add(trainer);
//        break;
//        }
//        }
//
//        // Print the data for each entity (for demonstration)
//        System.out.println("Users: " + users);
//        System.out.println("Trainees: " + trainees);
//        System.out.println("Trainings: " + trainings);
//        System.out.println("TrainingTypes: " + trainingTypes);
//        System.out.println("Trainers: " + trainers);
//        } catch (IOException e) {
//        e.printStackTrace();
//        }