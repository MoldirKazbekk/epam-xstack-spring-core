package com.epam.epamxstackspringcore.service;

import com.epam.epamxstackspringcore.dao.UserDAO;
import com.epam.epamxstackspringcore.entity.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {
    @Autowired
    private UserDAO userDAO;

    public List<User> getUsers() {
        return userDAO.getUsers();
    }

    public User createUser(User user) {
        user.setUsername(generateUsername(user.getFirstName(), user.getLastName()));
        user.setPassword(generateRandomString(10));
        return userDAO.createUser(user);
    }

    public User getUserById(String id){
        return userDAO.getUserById(id);
    }
    public boolean deleteUser(String id){
        return userDAO.deleteUserById(id);
    }

    public String generateUsername(String firstName, String lastName) {
        String username = firstName + "." + lastName;
        List<User> existedNameUsersList = userDAO.getUsersByFirstAndLastNames(firstName, lastName);
        int usersCount = existedNameUsersList.size();
        if (usersCount == 0) {
            return username;
        } else {
            return username + usersCount;
        }
    }
    public static String generateRandomString(int length) {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < length; i++) {
            int index = random.nextInt(characters.length());
            char randomChar = characters.charAt(index);
            stringBuilder.append(randomChar);
        }

        return stringBuilder.toString();
    }
}
