package com.epam.epamxstackspringcore.service;

import com.epam.epamxstackspringcore.dao.TraineeDAO;
import com.epam.epamxstackspringcore.entity.Trainee;
import com.epam.epamxstackspringcore.entity.User;
import com.epam.epamxstackspringcore.entity.dto.TraineeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class TraineeService {
    @Autowired
    private TraineeDAO traineeDAO;
    @Autowired
    private UserService userService;

    public List<Trainee> getTrainees() {
        return traineeDAO.getTrainees();
    }

    public void createTrainee(TraineeDTO traineeDTO) {
        Trainee trainee = new Trainee();
        User user = new User();
        user.setFirstName(traineeDTO.getFirstName());
        user.setLastName(traineeDTO.getLastName());
        user.setIsActive(true);
        user = userService.createUser(user);
        trainee.setAddress(traineeDTO.getAddress());
        trainee.setDateOfBirth(traineeDTO.getDateOfBirth());
        trainee.setUserId(user.getId());
        traineeDAO.createTrainee(trainee);
    }

    public Boolean deleteTraineeById(String id) {
        Trainee trainee = traineeDAO.deleteTraineeById(id);
        if (trainee == null) {
            log.info("trainee with id - {} doesn't exist and not deleted", id);
            return false;
        }
        return userService.deleteUser(trainee.getUserId());
    }

    public void updateTrainee(String id, TraineeDTO traineeDTO) {
        Trainee trainee = traineeDAO.getTraineeById(id);
        if (trainee == null) {
            return;
        }
        trainee.setDateOfBirth(traineeDTO.getDateOfBirth());
        trainee.setAddress(traineeDTO.getAddress());
//        traineeDAO.updateTrainee(trainee);
        User user = userService.getUserById(trainee.getUserId());
        if (user == null) {
            return;
        }
        user.setFirstName(traineeDTO.getFirstName());
        user.setLastName(traineeDTO.getLastName());
    }
}
