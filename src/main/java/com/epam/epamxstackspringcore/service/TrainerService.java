package com.epam.epamxstackspringcore.service;

import com.epam.epamxstackspringcore.dao.TrainerDAO;
import com.epam.epamxstackspringcore.dao.UserDAO;
import com.epam.epamxstackspringcore.entity.Trainer;
import com.epam.epamxstackspringcore.entity.User;
import com.epam.epamxstackspringcore.entity.dto.TrainerDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class TrainerService {
    @Autowired
    private TrainerDAO trainerDAO;
    @Autowired
    private UserService userService;

    public List<Trainer> getTrainers() {
        return trainerDAO.getTrainers();
    }

    public void createTrainer(TrainerDTO trainerDTO) {
        User user = new User();
        user.setFirstName(trainerDTO.getFirstName());
        user.setLastName(trainerDTO.getLastName());
        user.setIsActive(true);
        user = userService.createUser(user);
        Trainer trainer = new Trainer();
        trainer.setSpecialization(trainerDTO.getSpecialization());
        trainer.setUserId(user.getId());
        trainerDAO.createTrainer(trainer);
    }

    public void updateTrainer(String id, TrainerDTO trainerDTO) {
        Trainer trainer = trainerDAO.getTrainerById(id);
        if (trainer == null) {
            log.warn("there is no trainer by id {} for update function", id);
            return;
        }
        trainer.setSpecialization(trainerDTO.getSpecialization());
        User user = userService.getUserById(trainer.getUserId());
        user.setFirstName(trainerDTO.getFirstName());
        user.setLastName(trainerDTO.getLastName());
    }
}
