package com.epam.epamxstackspringcore.service;

import com.epam.epamxstackspringcore.dao.TraineeDAO;
import com.epam.epamxstackspringcore.dao.TrainerDAO;
import com.epam.epamxstackspringcore.dao.TrainingDAO;
import com.epam.epamxstackspringcore.dao.TrainingTypeDAO;
import com.epam.epamxstackspringcore.entity.Trainee;
import com.epam.epamxstackspringcore.entity.Trainer;
import com.epam.epamxstackspringcore.entity.Training;
import com.epam.epamxstackspringcore.entity.TrainingType;
import com.epam.epamxstackspringcore.entity.dto.TrainingDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class TrainingService {
    @Autowired
    private TrainingDAO trainingDAO;
    @Autowired
    private TrainerDAO trainerDAO;
    @Autowired
    private TraineeDAO traineeDAO;
    @Autowired
    private TrainingTypeDAO trainingTypeDAO;

    public List<Training> getTrainingList() {
        return trainingDAO.getTrainings();
    }

    public Boolean createTraining(TrainingDTO trainingDTO) {
        Trainer trainer = trainerDAO.getTrainerById(trainingDTO.getTrainerId());
        Trainee trainee = traineeDAO.getTraineeById(trainingDTO.getTraineeId());
        TrainingType trainingType = trainingTypeDAO.getTrainingTypeById(trainingDTO.getTrainingTypeId());
        if (trainee == null || trainer == null || trainingType == null) {
            log.error("one of id's doesn't exist: {}, {}, {}", trainingDTO.getTrainingTypeId(), trainingDTO.getTraineeId(), trainingDTO.getTrainerId());
            return false;
        }
        Training training = new Training();
        training.setDuration(trainingDTO.getDuration());
        training.setTraineeId(trainingDTO.getTraineeId());
        training.setTrainerId(trainingDTO.getTrainerId());
        training.setTrainingTypeId(trainingDTO.getTrainingTypeId());
        training.setName(trainingDTO.getTrainingName());
        trainingDAO.createTraining(training);
        return true;
    }
}
