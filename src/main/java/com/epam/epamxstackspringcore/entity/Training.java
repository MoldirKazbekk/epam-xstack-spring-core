package com.epam.epamxstackspringcore.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor

public class Training {
    private String id;
    private String traineeId;
    private String trainerId;
    private String name;
    private String trainingTypeId;
    private String date;
    private Number duration;

    public void setDuration(String duration) {
        this.duration = Long.valueOf(duration);
    }

    public void setDuration(Number duration) {
        this.duration = duration;
    }
}
