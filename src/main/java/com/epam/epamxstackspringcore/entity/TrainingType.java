package com.epam.epamxstackspringcore.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TrainingType {
    private String id;
    private String typeName;
}
