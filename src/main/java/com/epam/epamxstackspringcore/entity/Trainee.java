package com.epam.epamxstackspringcore.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@NoArgsConstructor
@Slf4j
public class Trainee {
    private String id;
    private Date dateOfBirth;
    private String address;
    private String userId;
    public void setDateOfBirth(String dateString){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = dateFormat.parse(dateString);
            this.dateOfBirth = date;
        } catch (ParseException e) {
            log.error("error while parsing date string in Trainee entity: {}", dateString);
        }
    }
}
