package com.epam.epamxstackspringcore.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor

public class User {
    private String id;
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private Boolean isActive;
    public void setIsActive(String isActive){
        this.isActive = Boolean.valueOf(isActive);
    }
    public void setIsActive(Boolean isActive){
        this.isActive = isActive;
    }
}
