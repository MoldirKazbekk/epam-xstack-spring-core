package com.epam.epamxstackspringcore.entity.dto;

import lombok.Data;

@Data
public class TrainingDTO {
    private String trainerId;
    private String traineeId;
    private String trainingName;
    private String trainingTypeId;
    private String trainingDate;
    private Integer duration;
}
