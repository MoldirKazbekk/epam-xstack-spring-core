package com.epam.epamxstackspringcore.entity.dto;

import lombok.Data;

@Data
public class TrainerDTO {
    private String firstName;
    private String lastName;
    private String specialization;
}
