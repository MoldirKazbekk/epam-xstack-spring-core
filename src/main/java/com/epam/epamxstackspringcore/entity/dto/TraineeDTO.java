package com.epam.epamxstackspringcore.entity.dto;

import lombok.Data;

@Data
public class TraineeDTO {
    private String firstName;
    private String lastName;
    private String address;
    private String dateOfBirth;
}
