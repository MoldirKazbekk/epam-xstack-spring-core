package com.epam.epamxstackspringcore.controller;

import com.epam.epamxstackspringcore.entity.Trainer;
import com.epam.epamxstackspringcore.entity.dto.TrainerDTO;
import com.epam.epamxstackspringcore.service.TrainerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/trainer")
@RequiredArgsConstructor
@Slf4j
public class TrainerController {
    private final TrainerService trainerService;

    @GetMapping("/all")
    public ResponseEntity<List<Trainer>> getTrainers() {
        return ResponseEntity.ok(trainerService.getTrainers());
    }

    @PostMapping
    public void createTrainer(@RequestBody TrainerDTO trainerDTO) {
        log.info("creating trainer entity: {}", trainerDTO);
        trainerService.createTrainer(trainerDTO);
    }

    @PutMapping
    public void updateTrainer(@RequestParam("id") String id,
                              @RequestBody TrainerDTO trainerDTO){
        log.info("updating trainer info by id - {}, new values {}", id, trainerDTO);
        trainerService.updateTrainer(id, trainerDTO);
    }
}
