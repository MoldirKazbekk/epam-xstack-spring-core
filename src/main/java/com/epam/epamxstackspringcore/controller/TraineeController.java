package com.epam.epamxstackspringcore.controller;

import com.epam.epamxstackspringcore.entity.Trainee;
import com.epam.epamxstackspringcore.entity.dto.TraineeDTO;
import com.epam.epamxstackspringcore.service.TraineeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/trainee")
@RequiredArgsConstructor
@Slf4j
public class TraineeController {
    private final TraineeService traineeService;

    @GetMapping("/all")
    public ResponseEntity<List<Trainee>> getTrainees() {
        return ResponseEntity.ok(traineeService.getTrainees());
    }

    @PostMapping
    public void createTrainee(@RequestBody TraineeDTO traineeDTO) {
        log.info("creating trainee: {}", traineeDTO.toString());
        traineeService.createTrainee(traineeDTO);
    }

    @PutMapping
    public void updateTrainee(@RequestParam("id") String id, @RequestBody TraineeDTO traineeDTO) {
        log.info("updating trainee by id - {} with values - {}", id, traineeDTO.toString());
        traineeService.updateTrainee(id, traineeDTO);
    }

    @DeleteMapping
    public ResponseEntity<Boolean> deleteTrainee(@RequestParam("id") String id) {
        log.warn("deleting trainee by id: {}", id);
        return ResponseEntity.ok(traineeService.deleteTraineeById(id));
    }
}
