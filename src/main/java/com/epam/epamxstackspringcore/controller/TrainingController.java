package com.epam.epamxstackspringcore.controller;

import com.epam.epamxstackspringcore.entity.Training;
import com.epam.epamxstackspringcore.entity.dto.TrainingDTO;
import com.epam.epamxstackspringcore.service.TrainingService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/training")
@RequiredArgsConstructor
@Slf4j
public class TrainingController {
    private final TrainingService trainingService;

    @GetMapping("/all")
    public ResponseEntity<List<Training>> getTrainingList() {
        return ResponseEntity.ok(trainingService.getTrainingList());
    }
    @PostMapping
    public ResponseEntity<Boolean> createTraining(@RequestBody TrainingDTO trainingDTO){
        log.info("creating training: {}", trainingDTO);
        return ResponseEntity.ok(trainingService.createTraining(trainingDTO));
    }
}
